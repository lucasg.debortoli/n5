import baseLocators from "../locators/baseLocators";

class BasePage {
    // Método común para navegar a una página
    startPage(url) {
        cy.viewport(1920, 1080);
        cy.visit(url, {headers: { "Accept-Encoding": "gzip, deflate" }});
        baseLocators.close_change_to_country_modal().then(($button) => {
            // Click on element if exists
            if($button.length > 0) {
                cy.wrap($button).click();
            }
        });
    }
}

export default BasePage;
