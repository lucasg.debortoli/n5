import BasePage from './basePage';
import homePageLocators from '../locators/homePageLocators';

class HomePage extends BasePage {
    visit() {
        this.startPage('https://www.latamairlines.com/co/es');
    }
    searchFlight(origin, destination) {
        cy.get('#txtInputOrigin_field').type(origin);
        cy.get('#btnItemAutoComplete_0').click();
        cy.get('#txtInputDestination_field').type(destination);
        cy.get('#btnItemAutoComplete_0').click();
        cy.get('#btnAddPassengerCTA').click();
        cy.get('[id="btnPlusChildren"]').click();
        cy.get('#btnTripTypeCTA').click();
        cy.contains('Solo ida').click();
        cy.get('#btnCabinTypeCTA').click();
        cy.contains('Premium Business').click();
        cy.get('#departureDate').click();
        homePageLocators.first_day().click();
        cy.get('#btnSearchCTA').click();
        cy.url().should('include', 'booking');
    }

}

export default new HomePage();
