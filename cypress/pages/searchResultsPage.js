import BasePage from './basePage';

class SearchResultsPage extends BasePage {

    visit() {
        this.startPage('https://www.latamairlines.com/co/es/ofertas-vuelos?origin=MDE&inbound=null&outbound=2023-12-18T15%3A00%3A00.000Z&destination=BOG&adt=1&chd=1&inf=0&trip=OW&cabin=Economy&redemption=false&sort=RECOMMENDED');
    }

    selectSeats() { /* ... */ }
}

export default new SearchResultsPage();
