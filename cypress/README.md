Agrego un test hasta la selección de fecha, luego de eso la página de latam está evitando
que acceda a la selección de vuelos. Probablemente sea un método contra scrappers de páginas
comparadoras de precio de vuelos de distintas aerolíneas. No pude descifrar qué headers/cookies 
tengo que agregar para poder validar eso. Otra cosa es que tienen que ser dos tests distintos, uno
hasta hacer click en Buscar y otro a partir de la url con los vuelos, eso porque Cypress no soporta
múltiples pestañas en el navegador a la vez.