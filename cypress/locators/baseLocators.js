class BaseLocators {
    close_change_to_country_modal() {
        return cy.get('[data-testid="country-suggestion--dialog-close-button"] > .sc-fzoxKX').should((_) => {});
    };
}

export default new BaseLocators();