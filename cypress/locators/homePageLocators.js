
class HomePageLocators {
    close_change_to_country_modal() {
        return cy.get('[data-testid="country-suggestion--dialog-close-button"] > .sc-fzoxKX');
    };

    first_day() {
        return cy.get('.CalendarDay__today', { force: true });
    }
}

export default new HomePageLocators();
