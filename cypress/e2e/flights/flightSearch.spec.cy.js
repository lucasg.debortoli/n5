import homePage from '../../pages/homePage';

describe('Flight Search', () => {
    it('should search for a flight from Medellín to Bogotá', () => {
        homePage.visit();
        homePage.searchFlight('Medellín', 'Bogotá');
    });
});
