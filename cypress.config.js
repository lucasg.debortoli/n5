const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
    supportFile: false,
    chromeWebSecurity: false,
    userAgent: 'Mozilla/5.0 (X11; Linux x86_64; rv:120.0) Gecko/20100101 Firefox/120.0',
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});
